
const routes = [
  {
    path: '/',
    component: () => import('layouts/Layout.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/Search.vue')
      },
      {
        path: '/favourites',
        component: () => import('pages/Favourites.vue')
      },
      {
        path: '/instructions',
        component: () => import('pages/Instructions.vue')
      }
    ]
  },

];

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
