const state = {
  favourites: {

  }
};

const mutations = {
  addFavourite(state, payload) {
    let id = payload.id;
    let favourite = payload.add;

    state.favourites[id] = favourite;
  },
  removeFavourite(state, payload) {
    let id = payload.id;

    delete state.favourites[id];
  }
};

const actions = {
  addFavourite({ commit }, payload) {
    commit('addFavourite', payload);
  },
  removeFavourite({ commit }, payload) {
    commit('removeFavourite', payload);
  }
};

const getters = {
  getFavourites: (state) => {
    return state.favourites;
  },
  isFavourite: (state) => (id) => {
    if (state.favourites[id]) {
      return 1;
    }
    return 0;
  },
};

const setters = {

};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
  setters
}
